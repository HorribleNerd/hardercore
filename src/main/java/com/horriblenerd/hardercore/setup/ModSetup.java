package com.horriblenerd.hardercore.setup;

import com.horriblenerd.hardercore.Hardercore;
import com.horriblenerd.hardercore.commands.ModCommands;
import com.horriblenerd.hardercore.network.Networking;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber(modid = Hardercore.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class ModSetup {

    private static final Logger LOGGER = LogManager.getLogger();

    public static void init(final FMLCommonSetupEvent event) {
        Networking.registerMessages();
    }

    @SubscribeEvent
    public static void setupCommands(final FMLServerStartingEvent event) {
        LOGGER.debug("Adding commands...");
        ModCommands.register(event.getServer().getDataPackRegistries().func_240968_f_().getDispatcher());
    }

}
