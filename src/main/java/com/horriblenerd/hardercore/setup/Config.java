package com.horriblenerd.hardercore.setup;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;

import java.util.Map;

@Mod.EventBusSubscriber
public class Config {

    public static final String CATEGORY_GENERAL = "general";

    public static ForgeConfigSpec SERVER_CONFIG;
    public static ForgeConfigSpec CLIENT_CONFIG;

    public static ForgeConfigSpec.IntValue MAX_HEALTH;
    public static ForgeConfigSpec.IntValue MAX_LIVES;
    public static ForgeConfigSpec.BooleanValue NATURAL_REGENERATION;
    public static ForgeConfigSpec.BooleanValue COUNTDOWN_MODE;

    static {
        ForgeConfigSpec.Builder SERVER_BUILDER = new ForgeConfigSpec.Builder();
        ForgeConfigSpec.Builder CLIENT_BUILDER = new ForgeConfigSpec.Builder();

        SERVER_BUILDER.comment("General settings").push(CATEGORY_GENERAL);
        MAX_HEALTH = SERVER_BUILDER.comment("Max player health").defineInRange("max_health", 20, 1, 1024);

        SERVER_CONFIG = SERVER_BUILDER.build();
        CLIENT_CONFIG = CLIENT_BUILDER.build();
    }

    public static void set(ForgeConfigSpec spec, String path, int newVal) {
        Map<String, Object> map = spec.valueMap();
        Object o = map.get(path);
        if (o instanceof ForgeConfigSpec.IntValue){
            ForgeConfigSpec.IntValue val = (ForgeConfigSpec.IntValue) o;
            val.set(newVal);
        }
    }

    public static void set(ForgeConfigSpec spec, String path, boolean newVal) {
        Map<String, Object> map = spec.valueMap();
        Object o = map.get(path);
        if (o instanceof ForgeConfigSpec.BooleanValue){
            ForgeConfigSpec.BooleanValue val = (ForgeConfigSpec.BooleanValue) o;
            val.set(newVal);
        }
    }

}
