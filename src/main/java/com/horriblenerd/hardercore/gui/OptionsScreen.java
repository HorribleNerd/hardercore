package com.horriblenerd.hardercore.gui;

import com.horriblenerd.hardercore.Hardercore;
import com.horriblenerd.hardercore.network.Networking;
import com.horriblenerd.hardercore.network.PacketOption;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.client.gui.widget.Slider;

public class OptionsScreen extends Screen {

    private static final int WIDTH = 179;
    private static final int HEIGHT = 151;
    private static Slider MAX_HEALTH;

    private ResourceLocation GUI = new ResourceLocation(Hardercore.MODID, "textures/gui/options_gui.png");


    public OptionsScreen() {
        super(new TranslationTextComponent("screen.hardercore.spawn"));
    }

    // Init
    @Override
    protected void func_231160_c_() {
        int relX = (this.field_230708_k_ - WIDTH) / 2;
        int relY = (this.field_230709_l_ - HEIGHT) / 2;

        MAX_HEALTH = new Slider(relX + 10, relY + 10, 150, 20, new StringTextComponent("Max Health: "), new StringTextComponent(""), 1, 1024, 20, false, true, (b) -> {});
        func_230480_a_(MAX_HEALTH);

        func_230480_a_(new Button(relX + 10, relY + 37, 160, 20, new StringTextComponent("Set"), button -> changeAll()));

//        func_230480_a_(new Button(relX + 10, relY + 10, 160, 20, new StringTextComponent("Skeleton"), button -> spawn("minecraft:skeleton")));
//        func_230480_a_(new Button(relX + 10, relY + 37, 160, 20, new StringTextComponent("Zombie"), button -> spawn("minecraft:zombie")));
//        func_230480_a_(new Button(relX + 10, relY + 64, 160, 20, new StringTextComponent("Cow"), button -> spawn("minecraft:cow")));
//        func_230480_a_(new Button(relX + 10, relY + 91, 160, 20, new StringTextComponent("Sheep"), button -> spawn("minecraft:sheep")));
//        func_230480_a_(new Button(relX + 10, relY + 118, 160, 20, new StringTextComponent("Chicken"), button -> spawn("minecraft:chicken")));
    }

    // isPauseScreen
    @Override
    public boolean func_231178_ax__() {
        return false;
    }

//    private void spawn(String id) {
//        Networking.sendToServer(new PacketSpawn(new ResourceLocation(id), field_230706_i_.player.world, field_230706_i_.player.getPositionVec()));
//        field_230706_i_.displayGuiScreen(null);
//    }

    private void changeAll() {
        Networking.sendToServer(new PacketOption(MAX_HEALTH.suffix.getString(), MAX_HEALTH.getValueInt()));
        field_230706_i_.displayGuiScreen(null);
    }

    // Render
    @Override
    public void func_230430_a_(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.field_230706_i_.getTextureManager().bindTexture(GUI);
        int relX = (this.field_230708_k_ - WIDTH) / 2;
        int relY = (this.field_230709_l_ - HEIGHT) / 2;
        // blit
        this.func_238468_a_(matrixStack, relX, relY, 0, 0, WIDTH, HEIGHT);
        super.func_230430_a_(matrixStack, mouseX, mouseY, partialTicks);
    }

    public static void open() {
        Minecraft.getInstance().displayGuiScreen(new OptionsScreen());
    }
}
