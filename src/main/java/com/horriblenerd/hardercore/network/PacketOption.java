package com.horriblenerd.hardercore.network;

import com.horriblenerd.hardercore.setup.Config;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.Supplier;

public class PacketOption {

    private static final Logger LOGGER = LogManager.getLogger();

    private final String path;
    private final int val;

    public PacketOption(PacketBuffer buf) {
        path = buf.readString();
        val = buf.readVarInt();
    }

    public PacketOption(String path, int val) {
        this.path = path;
        this.val = val;
    }

    public void toBytes(PacketBuffer buf) {
        buf.writeString(path);
        buf.writeVarInt(val);
    }

    public boolean handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            Config.set(Config.SERVER_CONFIG, path, val);
            LOGGER.debug(path);
            LOGGER.debug(val);
            Config.SERVER_CONFIG.save();
        });
        return true;
    }

}
