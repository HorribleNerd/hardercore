package com.horriblenerd.hardercore.commands;

import com.horriblenerd.hardercore.Hardercore;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.tree.LiteralCommandNode;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;

public class ModCommands {

    public static void register(CommandDispatcher<CommandSource> dispatcher) {
        LiteralCommandNode<CommandSource> cmdHC = dispatcher.register(
                Commands.literal(Hardercore.MODID)
                    .then(CommandOptions.register(dispatcher))
                    .then(CommandStart.register(dispatcher))
        );

        dispatcher.register(Commands.literal("hc").redirect(cmdHC));
    }

}
