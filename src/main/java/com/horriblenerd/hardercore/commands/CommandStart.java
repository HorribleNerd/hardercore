package com.horriblenerd.hardercore.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.GameRules;

public class CommandStart implements Command<CommandSource> {

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        LiteralArgumentBuilder<CommandSource> OPTIONS = Commands.literal("start")
                .requires(cs -> cs.hasPermissionLevel(2));
        GameRules.visitAll(new GameRules.IRuleEntryVisitor() {
            public <T extends GameRules.RuleValue<T>> void visit(GameRules.RuleKey<T> key, GameRules.RuleType<T> type) {
                OPTIONS
                        .then(Commands.literal(key.getName()).executes((src) -> CommandStart.get(src.getSource(), key))
                                .then(type.createArgument("value").executes((src) -> CommandStart.set(src, key))));
            }
        });
        return OPTIONS;
    }

    private static <T extends GameRules.RuleValue<T>> int set(CommandContext<CommandSource> src, GameRules.RuleKey<T> key) {
        CommandSource commandsource = src.getSource();
        T t = commandsource.getServer().getGameRules().get(key);
        t.updateValue(src, "value");
        commandsource.sendFeedback(new TranslationTextComponent("commands.gamerule.set", key.getName(), t.toString()), true);
        return t.intValue();
    }

    private static <T extends GameRules.RuleValue<T>> int get(CommandSource src, GameRules.RuleKey<T> key) {
        T t = src.getServer().getGameRules().get(key);
        src.sendFeedback(new TranslationTextComponent("commands.gamerule.query", key.getName(), t.toString()), false);
        return t.intValue();
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        return 0;
    }
}
